package recursion;

public class RecursionEjer {

	public static String resto(String s) {
		String res = "";
		for (int i = 1; i < s.length(); i++) {
			res = res + s.charAt(i);
		}
		return res;
	}
	
	public static String restof(String s) {
		String res = "";
		for (int i = 0; i < s.length() - 1; i++) {
			res = res + s.charAt(i);
		}
		return res;
	}



	public static String recortarA5(String s) {

		if (s.length() <= 5) {
			return s;
		}
		return recortarA5(restof(s));
	}



	public static boolean esAbecedaria(String s) {

		if (s.length() <= 1) {
			return true;
		}
		if (s.charAt(0) <= s.charAt(1)) {
			return esAbecedaria(resto(s));
		}
		return false;
	}


	public static String combinar(String s, String t) {
		if (s.length() == 0) {
			return t;
		}
		if (t.length() == 0) {
			return s;
		}
		if (s.toLowerCase().charAt(0) <= t.toLowerCase().charAt(0)) {
			return s.charAt(0) + combinar(resto(s), resto(t));
		} else {
			return t.charAt(0) + combinar(resto(s), resto(t));
		}
	}
	
	public static int sumarElementos(int[] elementos) {
		if(elementos.length == 0) {
			return 0;
		}
		return elementos[elementos.length-1] + sumarElementos(elementos);
		
//		return 0;
	}
	
	public static String espejar(String s) {
		if(s.length()==0) {
			return "";
		}
		
		return s.charAt(0) + espejar(resto(s)) + s.charAt(0);
	}
	
	public static boolean esVocal(char c) {
		return c=='a' || c=='e' || c=='i' || c=='o' || c=='u';
	}
	
	public static String repetirLetras(String s) {
		
		if (s.length()<=1) {
			return s;
		}

		if (esVocal(s.charAt(0)) && esVocal(s.charAt(1))) {

			return "" + s.charAt(0) + s.charAt(0) + repetirLetras(resto(s));

		} else {

		if (!esVocal(s.charAt(0)) && !esVocal(s.charAt(1))) {

			return "" + s.charAt(0) + s.charAt(0) + s.charAt(0) + repetirLetras(resto(s));

		}
		}
		return s.charAt(0) +repetirLetras(resto(s));
	}
	
	//Toma una cadena s, dos caracteres original y nuevo, devuelve nueva cadena que resulta de
	//remplazar en s todas las apariciones del caracter original por el caracter nuevo. 
	public static String remplazarCaracter (String s, char original, char nuevo) {
		
		
		if (s.length()==0) {
			return s;
		}
		
		if(s.charAt(0) == original) {
			return nuevo + remplazarCaracter(resto(s), original, nuevo);
		}
		
		return s.charAt(0) + remplazarCaracter(resto(s), original, nuevo);
	}
	
	public static int sumarElementos2(int[] elementos) {
		if (elementos.length == 0) {
			return 0;
		}
		int[] a = new int[elementos.length - 1];
		return elementos.length + sumarElementos2(a);
	}
	
	public static void main(String[] args) {
//		System.out.println(remplazarCaracter("casa", 'a', 'Q'));
		int[] a = {1,2,3,8};
        System.out.println(sumarElementos2(a));
		
	}
}
