package pcero;

import java.util.Scanner;

public class P0 {
	
//	6
	public static void imprimirSuma(int a, int b) {
		int suma = a + b;
		System.out.println("La suma es:" + suma);
	}
	
//	7
	public static void imprimirPromedio(int a, int b) {
		double promedio = (a+b)/2;
		System.out.println("El promedio es:" + promedio);
	}
	
//	8
	public static void ponerNota(double x, double y) {
		double promedio = (x+y)/2;
		if (promedio>=7) {
			System.out.println("Promocionado");
		}
		if (promedio>=4 && promedio<7) {
			System.out.println("Aprobado");
		}
		if (promedio<4) {
			System.out.println("Debe recuperar");
		}
	
	}
//	9
	public static void imprimirFecha(int dia, int mes, int anio) {
		String nombreMes;
		if (mes==1) {
			nombreMes = "Enero";
			System.out.println(dia + " de " + nombreMes + " de " + anio);
		} else {
		if (mes==2) {
			nombreMes = "Febrero";
			System.out.println(dia + " de " + nombreMes + " de " + anio);
		} else {
		if (mes==3) {
			nombreMes = "Marzo";
			System.out.println(dia + " de " + nombreMes + " de " + anio);
		} else {
		if (mes==4) {
			nombreMes = "Abril";
			System.out.println(dia + " de " + nombreMes + " de " + anio);
		} else {
		if (mes==5) {
			nombreMes = "Mayo";
			System.out.println(dia + " de " + nombreMes + " de " + anio);
		} else {
		if (mes==6) {
			nombreMes = "Junio";
			System.out.println(dia + " de " + nombreMes + " de " + anio);
		} else {
		if (mes==7) {
			nombreMes = "Julio";
			System.out.println(dia + " de " + nombreMes + " de " + anio);
		} else {
		if (mes==8) {
			nombreMes = "Agosto";
			System.out.println(dia + " de " + nombreMes + " de " + anio);
		} else {
		if (mes==9) {
			nombreMes = "Septiembre";
			System.out.println(dia + " de " + nombreMes + " de " + anio);
		} else {
		if (mes==10) {
			nombreMes = "Octubre";
			System.out.println(dia + " de " + nombreMes + " de " + anio);
		} else {
		if (mes==11) {
			nombreMes = "Noviembre";
			System.out.println(dia + " de " + nombreMes + " de " + anio);
		} else {
		if (mes==12) {
			nombreMes = "Diciembre";
			System.out.println(dia + " de " + nombreMes + " de " + anio);
		}
		}
		}
		}
		}
		}
		}
		}
		}
		}
		}
		}	
	}
//	10
	public static int sumatoria(int n) {
		int sumatoria = 0;
		for (int i = 0; i <= n; i++) {
			sumatoria = sumatoria + i;
		}
		return sumatoria;
	}
	
//	11
	public static int sumatoriaPares(int n) {
		int sumatoria = 0;
		for (int i = 2; i <= n; i+=2) {
			sumatoria = sumatoria + i;
		}
		return sumatoria;
	}
	
//	12
	public static double potencia(double x, int a) {
		double potencia = 1;
		for (int i = 1; i <= a; i++) {
			potencia = potencia * x;
		}
		return potencia;
	}
	
//	13
	public static double factorial(int n) {
		double factorial = 1;
		for (int i = 1; i <= n; i++) {
			factorial = factorial * i;
		}
		return factorial;
	}
	
//	14
	public static int cantCifras(int n) {
		int cifrasN = n;
		int i = 1;
		while(cifrasN > 10) {
			i++;
			cifrasN = cifrasN/10;
		}
		return i;
	}
	
//	15
	public static boolean esDivisible(int n, int m) {
		if (n%m == 0) {
			return true;
		} else {
			return false;
		}
	}
	
//	16. b.
//	public static void imprimirReversa(String cadena) {
//		String revesCad ="";
//		for(int i = cadena.length()-1; i >= 0; i = i-1) {
//			revesCad = revesCad + cadena.charAt(i);
//			}
//			System.out.println(revesCad);
//	}
	
//	16.c.
	public static String reversa(String cadena) {
		String revesCad ="";
		for(int i = cadena.length()-1; i >= 0; i = i-1) {
			revesCad = revesCad + cadena.charAt(i);
			}
		return revesCad;
	}
//	16. d.
	public static void imprimirReversa(String cadena) {
		System.out.println(reversa(cadena));
	}
	
//	17.
	public static int cantidadApariciones(String s, char c) {
		int k = 0;
		for (int i = 0; i < s.length(); i+=1) {
			if(s.charAt(i) == c) {
				k+=1;
			}
		}
		return k;
	}
	
//	18.
	public static int cantidadVocales(String s) {
		int k = 0;
		k = cantidadApariciones(s,'a') + cantidadApariciones(s,'e') + cantidadApariciones(s,'i') + cantidadApariciones(s,'o') + cantidadApariciones(s,'u');
		return k;
	}
	
//	19
	public static int posEnAbecedario(String s, int c) {
		String abc = "abcdefghijklmnopqrstuvwxyz";
		char k = s.charAt(c);
		int pos = 0;
		for(int i = 0; i < abc.length(); i+=1) {
			if(k == abc.charAt(i)) {
				pos = i;
			}		
		}
		return pos;
	}
	
//	19. 2.
	public static boolean esAbecedaria(String s) {
		boolean a = true;
		for(int i = 1; i < s.length(); i+=1) {
			
//			System.out.println(posEnAbecedario(s,i));
//			System.out.println(posEnAbecedario(s,i-1));
//			System.out.println(posEnAbecedario(s,i));
			if (posEnAbecedario(s,i-1) > posEnAbecedario(s,i)) {
//				System.out.println(posEnAbecedario(s,i-1));
//				System.out.println(posEnAbecedario(s,i));
				a = false;
				
			} 
			
		}
		return a;
	
	}
	
//	20.
	public static boolean esCapicua(String s) {
		boolean a = true;
		String sReversa = reversa(s); 
		if(s.equals(sReversa)) {
			a = true;
		}else {
			a = false;
		}
		return a;
	}
	
//	21
	public static boolean esSinRepetidos(String s) {
		for (int i=0; i<s.length(); i++) {
			for (int j=i+1; j<s.length(); j++) {
				if (s.charAt(i) == s.charAt(j)) {
					return false;
				}
			}
		}
		return true;
	}
	
//	22
	public static String sinRepetidos(String s) {
		String b="";
		for (int i=0; i<s.length(); i++) {
			if(cantidadApariciones(b,s.charAt(i))<1) {
				b = b + s.charAt(i);
			}
		}
		return b;
			
	}
	
//	25
	public static int maximo(int[] a) {
		int max = a[0];
		for (int i=0; i<a.length; i++) {
			if (max < a[i]) {
				max = a[i];
			}
		}
		return max;
	}
	
//	26
	public static int maximoIndice(int[] a) {
		int max = a[0];
		int j = 0;
		for (int i=0; i<a.length; i++) {
			if (max < a[i]) {
				max = a[i];
				j = i;
			}
		}
		return j;
	}
	
//	27
	public static double suma(double[] a) {
		double suma = 0;
		for (int i=0; i<a.length; i++) {
			suma = suma + a[i]; 
		}
		return suma;
	}
	
//	28
	public static boolean estaOrdenado(int[] a) {
		for (int i=0; i<a.length-1; i++) {
			if (a[i]>a[i+1]) {
				return false;
			}
		}
		return true;
		
	}
	
//	29
	public static double promedio(double[] a) {
		double k = suma(a);
		double prom = k / a.length;
		return prom;
	}
	
//	30.a.
	public static int sumatoriaRec(int n) {
		  if (n==0) {
		   return 0;
		  }
		  return n + sumatoriaRec(n-1);
		 }

//	30.b.
	public static int sumatoriaParesRec(int n) {
		if (n==0) {
			return 0;
		} 
		if (n%2==0) {
			return n+ sumatoriaParesRec(n-1);
		}
		return  sumatoriaParesRec(n-1);
	}
	
//	30.c.
	public static double potenciaRec(double x, int n) {
		if (n==0) {
			return 1;
		} else {
			double recursion = potenciaRec(x, n-1);
			double resultado = x * recursion;
			return resultado;
		}
		}
		
//	30.d.
	public static int factorialRec(int n) {
		if (n==0) {
			return 1;
		}else {
			int recursion = factorialRec(n-1);
			int resultado = n * recursion;
			return resultado;
		}
	}

//	31.
	public static int  fibrec(int n) {
		if(n==0) {
			return 0;
		}
		if (n==1) {
			return 1;
		}
		int recursion1 = fibrec(n-2);
		int recursion2 = fibrec(n-1);
		int resultado = recursion1 + recursion2;
		return resultado;
	}
	
//	31.b.
	public static int fibiter(int n) {
		if (n == 0) {
			return 0;
		}else if (n == 1) {
			return 1;
		}
		int[] a = new int[n+1];
		a[0]= 0;
		a[1]=1;
		for (int i = 2; i < a.length; i++) {
			a[i] = a[i-2] + a[i-1];
		}
		return a[n];
//		return a[n-2] + a[n-1];
	}
	
//	32
	public static void Collatz(int n) {
		if(n==1) {
			System.out.println(n);
			return;
		}
		if(n%2==0) {
			System.out.println(n);
			Collatz(n/2);
		} 
		if(n%2!=0) {
			System.out.println(n);
			Collatz((3*n)+1);
		} 
		
	}
	
//	33
	public static int mcd(int a, int b) {
		int r = a%b;
		if(r==0) {
			return b;
		}
		return mcd(b,r);
	}
	
//	34
	public static String resto(String s) {
		String b = "";
		for (int i = 1; i<s.length(); i++) {
			b = b + s.charAt(i);
		}
		return b;
	}
	
	public static String asterisco(String s) {
		String b ="";
		if(s.length()==1) {
			return s;
		}
		 b = s.charAt(0) + "*" + asterisco(resto(s));
		return b;
	}
	
//	35
	public static String sinRepetidosContiguos(String s) {
		if (s.length()==1) {
			System.out.println(s.charAt(0));
			return null;
		} else if(resto(s).charAt(0)!=s.charAt(0)) {
			System.out.println(s.charAt(0));
		}
		return sinRepetidosContiguos(resto(s));
		
	}
	
	public static void main(String[] args) {
//		1
//		String saludo = "Hola";
//		saludo = saludo + ", mundo";
//		System.out.println(saludo);
		
//		2
//		Scanner input = new Scanner(System.in);
//		String nombre;
//
//		System.out.print("¿Cómo te llamas? ");
//
//		nombre = input.nextLine();
//		
//		System.out.print("Hola, " + nombre);
		
//		3
//		System.out.println("Ingrese dos numeros");
//		Scanner numeros = new Scanner(System.in);
//		double a;
//		double b;
//		a = numeros.nextFloat();
//		b = numeros.nextFloat();
//		double suma = a + b;
//		System.out.print("La suma es:" + suma);
		
//		4
//		System.out.println(1/2); // 0
//		System.out.println(1.0/2.0); // 0.5
//		System.out.println(1.0/2); // 0.5
//		System.out.println(1/2.0); // 0.5
////		System.out.println("1"/"2"); // / no definido para string
//		System.out.println(1+2); // 3, suma
//		System.out.println("1"+"2"); // 12, concatena
//		System.out.println(16/2*4); // divide a 16 por 2 y multiplica el resultado por 4
//		System.out.println(16/(2*4)); // divide a 16 por 2*4
		
//		5
//		System.out.println("Ingrese dos numeros");
//		Scanner numeros = new Scanner(System.in);
//		double a;
//		double b;
//		a = numeros.nextFloat();
//		b = numeros.nextFloat();
//		double promedio = (a + b)/2;
//		System.out.print("El promedio es:" + promedio);
		
//		6
//		System.out.println("Ingrese dos numeros");
//		Scanner numeros = new Scanner(System.in);
//		int a;
//		int b;
//		a = numeros.nextInt();
//		b = numeros.nextInt();
//		imprimirSuma(a, b);
		
//		7
//		System.out.println("Ingrese dos numeros");
//		Scanner numeros = new Scanner(System.in);
//		int a;
//		int b;
//		a = numeros.nextInt();
//		b = numeros.nextInt();
//		imprimirPromedio(a, b);
						
//		8
//		System.out.println("Ingrese dos numeros");
//		Scanner numeros = new Scanner(System.in);
//		double a;
//		double b;
//		a = numeros.nextFloat();
//		b = numeros.nextFloat();
//		ponerNota(a, b);
		
//		9
//		imprimirFecha(5,7,2030);
		
//		10
//		int sumatoria = 10;
//		System.out.println(sumatoria(sumatoria));
		
//		11
//		int sumatoria = 10;
//		System.out.println(sumatoriaPares(sumatoria));
		
//		12
//		double x = 2;
//		int a = 3;
//		System.out.println(potencia(x,8));
		
//		13
//		System.out.println(factorial(8));
		
//		14
//		System.out.println((cantCifras(2256)));
		
//		15
//		System.out.println(esDivisible(259,2));
		
//		16. a.
//		String a = "hola";
//		String b = "";
//		System.out.println(a.length());
//		System.out.println(a.charAt(0));
//		for(int i = a.length()-1; i >= 0; i = i-1) {
//		for (int i = 0; i<a.length(); i+=1) {
//			System.out.println(a.charAt(i));
//			b = a.charAt(i) + b;
//			b = b + a.charAt(i);
//		}
//		System.out.println(b);
		
//		16. b.
//		imprimirReversa("hola");
		
//		16. c.
//		System.out.println(reversa("hola"));
		
//		17.
//		System.out.println(cantidadApariciones("eren",'r'));
		
//		18.
//		System.out.println(cantidadVocales("Computador"));
		
//		19
//		String abc = "abcdefghijklmnopqrstuvwxyz";
//		String s = "agil";	
//		char k = s.charAt(1);
//		System.out.println(k);
//		for(int i = 0; i < abc.length(); i+=1) {
//			if(k == abc.charAt(i)) {
//				System.out.println(i);
//			}
//		}
	
//		System.out.println(posEnAbecedario(s, 1));
//		for(int i = 1; i < s.length(); i+=1) {
//			boolean a = true;
//			System.out.println(posEnAbecedario(s,i));
//			System.out.println(posEnAbecedario(s,i-1));
//			System.out.println(posEnAbecedario(s,i));
//			if (posEnAbecedario(s,i-1) > posEnAbecedario(s,i)) {
//				System.out.println(posEnAbecedario(s,i-1));
//				System.out.println(posEnAbecedario(s,i));
//				System.out.println("False");
//				
//			} else {
//				System.out.println("True");
//			}
//		}
//		System.out.println(esAbecedaria(s));
		
//		String s = "ata";
//		boolean a = true;
//		String sReversa = reversa(s); 
//		System.out.println(s);
//		System.out.println(reversa(s));
//		if(s.equals( reversa(s))) {
//			a = true;
//		}else {
//			a = false;
//		}
//		System.out.println(a);
		
//		System.out.println(esCapicua("anana"));
		String s = "casa";
		boolean a = true;
		int[] ent= {1,2,3,4};
//		for(int i = 0; i < s.length(); i+=1) {
//			for(int j = s.indexOf(s.charAt(i)); j < s.length(); j+=1) {
//				if (s.charAt(i) == s.charAt(j)) {
//					System.out.println("false");
//				}else {
//					System.out.println("true");
//				}
//			}
//		}
		
//		22
//		System.out.println(sinRepetidos("casa"));
//		int[] a= {2, 10, 3, 55, 5};
//		double[] b= {5.2,10.1,17.2};
//		System.out.println(resto("hola"));
//		System.out.println(asterisco("hola"));
//		sinRepetidosContiguos("aaaadddddssss");

		
	}
		

}
